//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */

class StorableField {

  constructor(name, storableName) {
    this.name = name;
    this.storableName = storableName;
  }

  /**
   * Allows to define a transformer function for model to storable transformation.
   * 
   * ```
   const field = StorableField.create('f', 'fs', false);
   field.addStorableTransformer(value => { return `transformed ${value}` });
   field.storable({f: 123})
   ```
   * 
   * @param {Function} fcn a transformer function which accepts a single value that 
   * is the value of the defined field on the model object and return the transfirmed value
   */
  addStorableTransformer(fcn) {
    this.storableTransformer = fcn;
  }

  /**
   * Allows to define a transformer function for storable to model transformation.
   * 
   * ```
   const field = StorableField.create('f', 'fs', false);
   field.addModelTransformer(value => { return `transformed_model ${value}` });
   field.model({f: 123})
   ```
   * 
   * @param {Function} fcn a transformer function which accepts a single value that 
   * is the value of the defined field on the model object and return the transfirmed value
   */
  addModelTransformer(fcn) {
    this.modelTransformer = fcn;
  }

  storable(model) {
    let value = model[this.name];
    if(this.storableTransformer) {
      return this.storableTransformer(value)
    }
    return value;
  }

  model(storable) {
    let value = storable[this.storableName]
    if(this.modelTransformer) {
      return this.modelTransformer(value)
    }
    return value;
  }

  /**
   * 
   * @param {String} name the field name on the model object
   * @param {String} storableName the field name on the upstream service
   */
  static create(name, storableName, isId) {
    return new StorableField(name, storableName);
  }
}

module.exports = StorableField;