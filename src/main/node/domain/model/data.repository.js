//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const Repository = require('dino-core').Repository;
const StorableContext = require('./storable.context');

/**
 * Abstraction of a data enetity that can be stored on a remote service or DB.xc 
 */
class DataRepository extends Repository {

  /**
   * Provides the context for the storable object, the context provides 
   * useful information on how to store the object.
   * 
   * Information that can be provided are:
   *  * the field that will act as a identifier
   *  * fields that should be stored
   *  * per-field transform function to allow data translation. 
   * 
   * By default, meaning the StorableContext is left empty the behaviour will be to
   * persist all fields as-is and delegate to the upstream service the generation of the 
   * identifier.
   * @returns {StorableContext} the storable context object
   */
  storableContext() {
    return StorableContext.create();
  }
}

module.exports = DataRepository;