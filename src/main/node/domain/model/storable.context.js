//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */

class StorableContext {

  constructor() {
    this.fields = new Map();
    this.storableFields = new Map();
  }

  addField(field) {
    this.fields.set(field.name, field);
    this.storableFields.set(field.storableName, field);
  }

  /**
   * Validate if the storable context is defined or not.
   * @returns {boolean} true if the storable context is not defined, false otherwise.
   */
  isEmpty() {
    return this.fields.size == 0;
  }

  /**
   * Translate the model object to a storable, the translation strategy is as following:
   * 
   * all fields part of the model object will be translated, for the one that a configuration 
   * field is defined, the provided confuguration is used.
   * 
   * @param {any} model the model object to translate
   * @returns {Object} the storable object
   */
  storable(model) {
    if (!model) {
      throw new Error('model is not defined, nothing can be stored');
    }
    return Object.keys(model).reduce((answer, property) => {
      if (this.fields.has(property)) {
        let field = this.fields.get(property);
        answer[field.storableName] = field.storable(model);
      } else {
        answer[property] = model[property];
      }
      return answer;
    }, {});
  }

  model(storable, type) {
    if (!storable) {
      return undefined;
    }
    return Object.keys(storable).reduce((answer, property) => {
      if (this.storableFields.has(property)) {
        let field = this.storableFields.get(property);
        answer[field.name] = field.model(storable);
      } else {
        let field = answer[property];
        if (field instanceof Map) {
          Object.entries(storable[property] || {}).forEach(entry => { answer[property].set(entry[0], entry[1]) });
        } else if (field instanceof Array) {
          (storable[property] || []).forEach(entry => { answer[property].push(entry) });
        } else {
          answer[property] = storable[property];
        }
      }
      return answer;
    }, new type());
  }

  static create() {
    return new StorableContext();
  }
}

module.exports = StorableContext;