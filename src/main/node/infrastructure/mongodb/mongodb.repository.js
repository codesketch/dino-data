//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */

const monogram = require('monogram');
const DataRepository = require('../../domain/model/data.repository');

class MongoDBRepository extends DataRepository {

  constructor({environment}) {
    super();
    this.environment = environment;
  }

  async postConstruct() {
    let db = await monogram.connect(
      this.environment.get('dino:data:mongodb:uri') || 'mongodb://localhost:27017', 
      this.environment.get('dino:data:mongodb:options') || {});
    this.collection = db.collection(this.environment.get('dino:data:mongodb:collection') || 'test')
  }

  /**
   * Save a model object, following the defined configuration to MongoDB.
   * @param {any} aggregateRoot the model to persist
   * @returns {any} the provided model object
   */
  async save(aggregateRoot) {
    let storable = this.storableContext().storable(aggregateRoot);
    await this.collection.insertOne(storable);
    return aggregateRoot;
  }

  async getBy(field, value, type) {
    let storable = await this.collection.findOne({[field]: value});
    return this.storableContext().model(storable, type);
  }

  async getAllBy(query, type) {
    let context = this.storableContext();
    let storables = await this.collection
      .find(query);
    return (storables || []).map(storable => context.model(storable, type));
  }

  async find(type) {
    let context = this.storableContext();
    let storables = await this.collection
      .find();
    return (storables || []).map(storable => context.model(storable, type));
  }
  
};

/**
 * Repository class for MongoDB
 * @typedef MongoDBRepository
 * @public
 */
module.exports = MongoDBRepository;