//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const InMemoryRepository = require('../../../../main/node/infrastructure/inmemory/in.memory.repository');

describe('In-Memory Repository', () => {
  const environment = {};
  let testObj = new InMemoryRepository({ environment });
  it('stores a document and is able to retrieve by id', async () => {
    await testObj.save(new Document(1, 'my doc'));
    // assert
    assert.ok(!! await testObj.getBy('id', 1, Document));
  });

  it('retrieve all documents', async () => {
    testObj.save(new Document(2, 'my doc 2'));
    // assert
    assert.equal((await testObj.find(Document)).length, 2);
  });

  it('retrieve documents by query', async () => {
    testObj.save(new Document(3, 'my doc 3'));
    // act
    let actual = await testObj.getAllBy('storable[\'description\']==\'my doc 3\'', Document)
    // assert
    assert.equal(actual.length, 1);
    assert.equal(actual[0].id, 3)
  });

});

class Document {
  constructor(id, description) {
    this.id = id;
    this.description = description;
  }
}