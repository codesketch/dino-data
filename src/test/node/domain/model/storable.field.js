//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const StorableField = require('../../../../main/node/domain/model/storable.field');

describe('Storable Field', () => {

  it('translate a model object without translator', () => {
    let testObj = StorableField.create('name', 'nm');
    // act
    let actual = testObj.storable({name: 'dino-data'});
    // assert
    assert.equal(actual, 'dino-data');
  });

  it('translate a model object with translator', () => {
    let testObj = StorableField.create('name', 'nm');
    testObj.addStorableTransformer(value => { return `transformed ${value}` });
    // act
    let actual = testObj.storable({name: 'dino-data'});
    // assert
    assert.equal(actual, 'transformed dino-data');
  });

  it('translate a storable object with translator', () => {
    let testObj = StorableField.create('name', 'nm');
    testObj.addModelTransformer(value => { return `transformed_storable ${value}` });
    // act
    let actual = testObj.model({nm: 'dino-data'});
    // assert
    assert.equal(actual, 'transformed_storable dino-data');
  });
});