//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const StorableContext = require('../../../../main/node/domain/model/storable.context');
const StorableField = require('../../../../main/node/domain/model/storable.field');

describe('Storable Context', () => {

  it('validate the context is empty', () => {
    // assert
    assert.ok(StorableContext.create().isEmpty());
  });

  it('validate the context is not empty', () => {
    let testObj = StorableContext.create();
    testObj.addField(StorableField.create('name', 'nm'))
    // assert
    assert.ok(!testObj.isEmpty());
  });

  it('translate a model object', () => {
    let testObj = StorableContext.create();
    testObj.addField(StorableField.create('name', 'nm'))
    // act
    let actual = testObj.storable({name: 'dino-data'});
    // assert
    assert.deepEqual(actual, { nm: 'dino-data' });
  });

  it('translate a model object from empty context from mixed definition fields', () => {
    let testObj = StorableContext.create();
    testObj.addField(StorableField.create('name', 'nm'))
    // act
    let actual = testObj.storable(new Test('name','description'));
    // assert
    assert.deepEqual(actual, { nm: 'name', description: 'description' });
  });

  it('translate a model object to storable', () => {
    let testObj = StorableContext.create();
    // act
    let actual = testObj.storable(new Test('name','description'));
    // assert
    assert.deepEqual(actual, { name: 'name', description: 'description' });
  });

  it('translate a storable object to model', () => {
    let testObj = StorableContext.create();
    // act
    let actual = testObj.model({ name: 'name', description: 'description' }, Test);
    // assert
    assert.deepEqual(actual, new Test('name','description'));
  });

  it('translate a storable object to model from mixed definition fields', () => {
    let testObj = StorableContext.create();
    testObj.addField(StorableField.create('name', 'nm'))
    // act
    let actual = testObj.model({ nm: 'name', description: 'description' }, Test);
    // assert
    assert.deepEqual(actual, new Test('name','description'));
  });

  class Test {
    constructor(name, description) {
      this.name = name;
      this.description = description;
    }
  }
});